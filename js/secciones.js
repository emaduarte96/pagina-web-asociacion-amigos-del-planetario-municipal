
///////*Inicio*//////

document.getElementById('inicio').addEventListener("click",function(){
    document.getElementById("total").innerHTML =`
    <div>
    <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
        
        <div class="carousel-inner">
          <div class="carousel-item active">
            <img src="img/descarga.jpg" class="d-block w-100" alt="225px">
            <div class="carousel-caption d-none d-md-block">
            </div>
          </div>
      </div> 
    </div>


    <div id="jumbo" class="jumbotron jumbotron-fluid">
    <div class="container">
      <h3 class="display-4 wow zoomIn">Horarios de inscripción a los cursos 2021</h3><br>
      <p class="lead wow zoomIn"><strong> Los esperamos Jueves y Viernes de 19:00 a 21:00 Hs. en el hall del observatorio. Las inscripciones son hasta el 26/3/2021. Los cursos arrancan la semana del 5/4/2021. ¡Los esperamos!</strong></p>
    </div>
   </div>`
});

///////*Cursos*//////

document.getElementById('cursos').addEventListener("click",function(){
    document.getElementById("total").innerHTML = `
    <div id="cursos" class="row columna_2">
    
    <div  class="col-12 col-md-12">
         <h1 id="titulo" class="wow slideInLeft">Cursos Anuales 2021</h1>
          <div class="row featurette">
            <div class="col-md-7 wow zoomIn">
              <h2 class="featurette-heading">INTRODUCCIÓN A LA ASTRONOMÍA</h2>
              <br>
              <p class="lead">Astronomía de posición (astronomía esférica y mecánica celeste). Astrofísica, la Luna, el Sol, galaxias, estrellas y nebulosas. Calendario. Planetología. Clases teóricas y Prácticas a cielo abierto.</p>
              <br><p class="text_p2"><b>Horarios: Viernes de 19:30 a 21:30 hs.</b></p>
            </div>
            <div class="col-md-5  wow zoomIn">
              <img class="img_cursos_2" src="img/intro.jpg">
            </div>
          </div>
          <hr class="featurette-divider">

          <div class="row featurette">
            <div class="col-md-7 order-md-2 wow zoomIn">
              <h2 class="titulo_curso" class="featurette-heading">MAPA DEL CIELO</h2>
              <p class="lead">Desarrollo del mapa del cielo (nombre de las estrellas, magnitudes, ubicación, constelaciones). Los diferentes cielos de diferentes ubicaciones y estaciones. Mitología griega, historia, zodíaco. Salidas observacionales cada mes. Software astronómico. Noticias y novedades sobre astronomía y astronáutica.</p>
              <br><p class="text_p2"><b>Horarios: Lunes de 19 a 21 hs.</b></p>
            </div>
            <div class="col-md-5 order-md-1 wow zoomIn">
             <img class="img_cursos" src="img/mapacielo.jpg">
            </div>
          </div>
          
          <hr class="featurette-divider">

          <div class="row featurette">
            <div class="col-md-7 wow zoomIn">
              <h2 class="featurette-heading">LA CIENCIA DEL UNIVERSO</h2>
              <p class="lead">La energía y el hombre. El Universo (estrellas, galaxias, agujeros negros, novas y supernovas, cuásares). Teoría de la relatividad. Teoría del caos y fractales. Física cuántica. Viajes en el tiempo. Universos paralelos. Cosmología. Navegación estelar.</p>
              <br><p class="text_p2"><b>Horarios: Martes de 19:30 a 21:30 hs.</b></p>
            </div>
            <div class="col-md-5 wow zoomIn">
              <img class="img_cursos" src="img/cienciauni.jpg">
            </div>
          </div>

          <hr class="featurette-divider">

          <div class="row featurette">
            <div class="col-md-7 order-md-2 wow zoomIn">
              <h2 class="featurette-heading">FÍSICOQUÍMICA APLICADA A LA ASTRONOMÍA</h2>
              <p class="lead">Fuerzas, Termodinámica, Electromagnetismo. Física Relativista y Cuántica. Teletransportación, Vida extraterrestre, Los peligros del viaje espacial, Animación suspendida, Modelos atómicos. Modelo estándar. La máquina de Dios y la partícula divina. Teoría de cuerdas. Evolución estelar. Fusión y fisión nuclear. Astrobiología.</p>
              <p class="text_p2"><b>Horarios: Jueves de 19:30 a 21:30 hs.</b></p>
            </div>
            <div class="col-md-5 order-md-1 wow zoomIn">
              <img class="img_cursos_2" src="img/fisicoqui.jpg">
            </div>
          </div>
          <hr class="featurette-divider">
          <p id="pie" class="wow zoomIn"><b>Los cursos están dirigidos al público en general y no es necesario poseer conocimientos previos.</b></p>
          
    </div>
  </div>`;
});

///////*Miembros*//////

document.getElementById('miembros').addEventListener("click",function(){
    document.getElementById("total").innerHTML =`
    <div  class="row miembros">

    <div class="principal col-12 col-md-12">
  
      <div id="hoja" class="sombra wow zoomIn">
        <div class="foto_cont"><img src="img/comision2016.jpeg" class="wow jackInTheBox" data-wow-delay = "0.5s"></div>
        <div><p>La Comisión Directiva de la Asociación Amigos del Observatorio Astronómico y Planetario Municipal de Rosario está integrada por los siguientes miembros:</p>
        <ul>
          <li>Presidente Miguel Scapini</li>
          <li>Vicepresidente Andrés Lencina</li>
          <li>Secretario Daniel Bozicovich</li>
          <li>Tesorero Fernando Venier</li>
          <li>1º Vocal Dino Cavaglia</li>
          <li>2º Vocal Marcela Guelfi</li>
          <li>Síndico Ricardo Giménez</li>
          <li>Síndico Suplente Cecilia Gorgas</li>
        </ul>
        </div>
      </div>
    </div>
    </div>  `
});

///////*Complejo Astronomico*//////

document.getElementById('complejo').addEventListener("click",function(){
    document.getElementById("total").innerHTML =`
    <div class="row comp_astro">
    
    <div class=" col-12 col-md-12 principal_complejo">
      <div id="hoja_1" class="sombra wow zoomIn">
      <div><h3>Complejo Astronómico Municipal</h3></div>
      <br>
        <p>El Complejo Astronómico Municipal se constituye en un espacio donde 
          la cultura y la ciencia se entrelazan generando una propuesta educativa, lúdica y participativa.</p>
          <ul>
            <li>Planetario</li>
            <li>Observatorio</li>
            <li>Museo Experimental de Ciencias</li>
          </ul>
        <h4>Funciones del Planetario:</h4>
        <p>El encanto y los enigmas que encierra el universo se proyectan en una cúpula semiesférica
           poniendo en juego la mecánica, la óptica y la electrónica.</p>
        <div class="img_exp1"><img src="img/funcion_plan.jpg"></div>
        <p id="pie_foto">Fotografia <b>Esmeralda Sosa</b></p>
        <h4>Museo Experimental de Ciencias:</h4>
        <br><div class="img_exp1"><img class="img_01" src="img/museo01.png"></div>
        <br>
        <div class="img_exp1"><img class="img_exp02" src="img/museo1.jpg"></div>
        <p id="pie_foto"><b>Parque Urquiza, 2000 Rosario, Argentina.</b></p>
        <br>
        <p>Vista del edificio Planetario. El sector anular del segundo piso corresponde al Museo Experimental de Ciencias Municipal de Rosario. Fotografía <b>Esmeralda Sosa</b>.</p>

        <p>Cuenta con las áreas de matemática, física, ciencia y tecnología, astronomía y astronáutica, geología, química y biología. Se pueden realizar, también, experiencias prácticas de óptica, luz láser, energía estática, propagación de las radiaciones electromagnéticas, observaciones en telescopio y microscopio.
          
          Para más información visite el sitio del Complejo Astronómico Municipal. </p>

      </div>
    </div>
 </div> `
});

///////*Video del cielo*//////

document.getElementById('video').addEventListener("click",function(){
    document.getElementById("total").innerHTML =`
    <div  class="row miembros">

    <div class="principal col-12 col-md-12">
  
      <div id="hoja" class="sombra wow zoomIn">
          <h3>El cielo del mes en el hemisferio sur</h3>
          <div class="ifr"><iframe class="wow zoomInDown" width="560" height="315" src="https://www.youtube.com/embed/eck3t6D1-3k" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>
          <p>Videos Cortesia del Canal de Youtube <b>“La Costa de las Estrellas”</b></p>
       </div>
      </div>
    
    </div>  `
});

///////*Archivos mapa del cielo*//////

document.getElementById('archivos').addEventListener("click",function(){
    document.getElementById("total").innerHTML =`
    <div  class="row miembros">

    <div class="principal col-12 col-md-12">
  
      <div id="hoja" class="sombra wow zoomIn">
          <h3 id="titulo_archivos">Archivos Mapa Del Cielo</h3>
          <ul id="lista_archivos_map"> 
              <li><a href="pdf/astrorosario2001.pdf" target="_blanck">Enero 2020</a></li>
              <li><a href="#" target="_blanck">Febrero 2020</a></li> 
              <li><a href="#" target="_blanck">Marzo 2020</a></li> 
              <li><a href="#" target="_blanck">Abril 2020</a></li>
              <li><a href="#" target="_blanck">Mayo 2020</a></li> 
              <li><a href="#" target="_blanck">Junio 2020</a></li> 
              <li><a href="#" target="_blanck">Julio 2020</a></li>
              <li><a href="#" target="_blanck">Agosto 2020</a></li> 
              <li><a href="#" target="_blanck">Septiembre 2020</a></li> 
              <li><a href="#" target="_blanck">Octubre 2020</a></li> 
              <li><a href="#" target="_blanck">Noviembre 2020</a></li> 
              <li><a href="#" target="_blanck">Diciembre 2020</a></li> 
            </ul>
        </div>
      </div>
  
    </div>  `
});

